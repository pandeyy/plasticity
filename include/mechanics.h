//
//Computational Mechanics and Multiphysics Group @ UW-Madison
//Created 2011
//authors: rudraa (2011, 2018)
//
#ifndef MECHANICS_H_
#define MECHANICS_H_
#include "functionEvaluations.h"
#include "supplementaryFunctions.h"
#include "deformationMap.h"


template <int dim>
struct historyVariables{
public:
  historyVariables<dim>():
  alpha(0.0), beta (dim, dim), betaIteration(dim, dim), deviator_Ep(dim,  dim), deviator_EpIteration(dim, dim){}

  //using std:::map to store time history variables
  double alpha, alphaIteration;
  dealii::Table<2, double > beta, betaIteration;
  dealii::Table<2, double > deviator_Ep, deviator_EpIteration;
};
//mechanics implementation
template <class T, int dim>
  void evaluateStress(unsigned int q, const unsigned int DOF, const Table<1, T>& ULocal, const deformationMap<T, dim>& defMap, unsigned int currentIteration,  std::vector<historyVariables<dim>*>& history, Table<2, double>& stress, Table<4,double>& C){ 

  //update history variables at the start of increment
  if (currentIteration==0){
    history[q]->alpha=history[q]->alphaIteration;
    history[q]->beta=history[q]->betaIteration;
    history[q]->deviator_Ep=history[q]->deviator_EpIteration;
  }
  //material properties
  Table<2,double> Fe (dim, dim);
  Table<2,double> E (dim, dim);
  Table<2,double> gradU (dim, dim);
  double Y=elasticModulus, nu=PoissonsRatio;
  //Lame parameters
  double lambda=(nu*Y)/((1+nu)*(1-2*nu)), mu=Y/(2*(1+nu));
  double kappa= lambda+(2.0/3.0)*mu;
  Table<2,double> dev_E(dim,dim);
  double trace_E=0;
  Table<2, double> dev_stress(dim,dim);
  Table<2,double>trial_xi(dim,dim);
  Table<2,double>beta(dim,dim), deviator_Ep(dim, dim);
  double alpha=0;
  double f_trial=0.0;
  double temp_xi=0.0,norm_xi=0.0;
  double H=0.0;
  double H_conv=0.0;//H using history[q]->alphaIteration;
  double K=1000;
  //double K_conv=0.1; // usign history[q]->alphaIteration;
  double tol=1e-6;
  double gamma=0.0;
  double g;
  double D_g;
  double theta=1;
  double mod_g;
  Table<2,double> n(dim,dim);
  //Fe
  for (unsigned int i=0; i<dim; ++i){
    for (unsigned int j=0; j<dim; ++j){
      // Fe[i][j]=defMap.F[q][i][j];
      gradU[i][j]=defMap.F[q][i][j] - (double)(i==j);//gradU
      n[i][j]=0.0;
    }
  }
  
  for(unsigned int i=0;i<dim;i++){
      for(unsigned int j=0;j<dim;j++){
        E[i][j] = 0.5*(gradU[i][j]+gradU[j][i]);// strain
	
      }
      trace_E=trace_E+E[i][i];
    }
  
  for(unsigned int i=0;i<dim;i++){
    for(unsigned int j=0;j<dim;j++){
      dev_E[i][j]=E[i][j]-(trace_E/3.0)*(double)(i==j);
      dev_stress[i][j]=2.0*mu*(dev_E[i][j]-history[q]->deviator_Ep[i][j]);//stress deviator
      //beta[i][j]=history[q]->beta[i][j];
      trial_xi[i][j]=dev_stress[i][j]-history[q]->beta[i][j];
      temp_xi+=std::pow(trial_xi[i][j],2);
    }
  }
  
  norm_xi=std::sqrt(temp_xi); 
  f_trial=norm_xi-std::sqrt(2./3.)*K;

  //check for elastic-plastic condition	 
  if(f_trial>0){
    for(unsigned int i=0;i<dim;i++)
      for(unsigned int j=0;j<dim;j++)
	n[i][j]=trial_xi[i][j]/norm_xi;
    
    //update plastic-slip
    g=((-1.)*std::sqrt(2./3.)*K)+(norm_xi)-(2*mu*gamma)-((std::sqrt(2.0/3.0))*(H-H_conv));
    D_g=-2.*mu*(1+((H-K)/(3*mu)));
    
    if(g<0)mod_g=-1*g; else mod_g=g;
    
    while(mod_g>tol){      
      gamma=gamma-(g/D_g);
      alpha= history[q]->alpha +( std::sqrt(2./3.))*gamma;
      //H=;
      //K=;
      g=(-1.0)*std::sqrt(2.0/3.0)*K+norm_xi-2*mu*gamma-std::sqrt(2.0/3.0)*(H-H_conv);
      D_g=-2.0*mu*(1+((H-K)/(3.0*mu)));
      if(g<0)mod_g=-1.0*g;
      else mod_g=g;	    
    }
    // update theta bases on gamma
    theta=1.0-((2*mu*gamma)/(norm_xi));
  }

  
  for(unsigned int i=0;i<dim;i++){
    for(unsigned int j=0;j<dim;j++){
      beta[i][j]=history[q]->beta[i][j]+std::sqrt(2./3.)*(H-H_conv)*n[i][j];
      deviator_Ep[i][j]= history[q]->deviator_Ep[i][j]+gamma*n[i][j];
      stress[i][j]=kappa*(i==j)*trace_E + dev_stress[i][j]-2.*mu*gamma*n[i][j];
    }
  }
  
   double theta_bar=-1.0+theta+(1.0/(1.0+(K+H)/(3.0*mu)));
  //define elasto-plastic tensor
  for(unsigned int i=0;i<dim;i++){
    for(unsigned int j=0;j<dim;j++){
      for(unsigned int k=0;k<dim;k++){
	for(unsigned int l=0;l<dim;l++){
	  C[i][j][k][l]=(kappa*(double)((i==j)*(k==l)))+2*mu*theta*(0.5*(double)((i==k)*(j==l)+(i==l)*(j==k))-(1./3.)*(i==j)*(k==l))-2*mu*theta_bar*n[i][j]*n[k][l];
	 
	}
      }
    }
  }
  
  //update iteration level history variables
  history[q]->alphaIteration=alpha;
  history[q]->betaIteration=beta;
  
  history[q]->deviator_EpIteration=deviator_Ep;
}

//mechanics residual implementation
template <int dim>
void residualForMechanics(FEValues<dim>& fe_values, unsigned int DOF, Table<1, double >& ULocal, Table<1, double>& ULocalConv, deformationMap<double, dim>& defMap, unsigned int currentIteration,  std::vector<historyVariables<dim>* >& history, Vector<double>& RLocal, FullMatrix<double>& KLocal){
  unsigned int dofs_per_cell= fe_values.dofs_per_cell;
  unsigned int n_q_points= fe_values.n_quadrature_points;
  
  //quadrature loop
  for (unsigned int q=0; q<n_q_points; q++){
    
    //evaluate stress
    Table<2, double> stress(dim, dim);
    Table<4,double> C(dim,dim,dim,dim);
    evaluateStress<double, dim>(q, DOF, ULocal, defMap, currentIteration, history, stress, C);

    //evaluate Residual
    for (unsigned int i=0; i<dofs_per_cell; i++) {
      const unsigned int ci = fe_values.get_fe().system_to_component_index(i).first - DOF;
      if (ci>=0 && ci<dim){
	// R = Grad(w)*P
	for (unsigned int di = 0; di<dim; di++){
	  RLocal(i) += -fe_values.shape_grad(i, q)[di]*(stress[ci][di])*fe_values.JxW(q);
	  //evaluate tangent
	  for (unsigned int j=0; j<dofs_per_cell; j++) {   
	    const unsigned int cj = fe_values.get_fe().system_to_component_index(j).first - DOF;
	    if (cj>=0 && cj<dim){
	      //K
	      for (unsigned int dj = 0; dj<dim; dj++){
		KLocal(i,j)+= fe_values.shape_grad(i,q)[di]*C[ci][di][cj][dj]*fe_values.shape_grad(j,q)[dj]*fe_values.JxW(q);
	      }
	    }
	  }
	}
      }
    }
    //end quadrature loop
  }
}

#endif /* MECHANICS_H_ */
