//problem geometry, mesh control
#define DIMS 2
#define problemWidth 1.0
#define refinementFactor 5

//mechanics properties
#define elasticModulus 1//2.0e11
#define PoissonsRatio 0.3

//time step controls
#define TimeStep 1.0
#define TotalTime 1000*TimeStep

//output controls
#define outputFileName "solution"
